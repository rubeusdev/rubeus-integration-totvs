<?php

namespace Rubeus\RbIntegrationTotvs\Src;

use Rubeus\RbIntegrationTotvs\Src\Services;

class Client
{
    private $clientSoap;

    private $tbc;
    private $totvs_user;
    private $totvs_pass;
    private $use_without_license;
    private int $connection_timeout = 1000;

    private $errorMsg;
    private $sentXml;
    private $resultResponse;

    public function __construct($tbc, $totvs_user, $totvs_pass, $use_without_license = false, int $connection_timeout = 1000)
    {
        $this->tbc = $tbc;
        $this->totvs_user = $totvs_user;
        $this->totvs_pass = $totvs_pass;
        $this->use_without_license = $use_without_license;
        $this->connection_timeout = $connection_timeout;
    }

    public function getLastError()
    {
        return $this->errorMsg;
    }

    public function getXmlSent()
    {
        return $this->sentXml;
    }

    public function getResultResponse()
    {
        return $this->resultResponse;
    }

    public function executeSqlQuery($codSentenca, $codColigada, $codSistema, $parameters, $context = null, $forceWithoutLicense = null)
    {
        $sentData = [
            'codSentenca' => $codSentenca,
            'codColigada' => $codColigada,
            'codSistema' => $codSistema,
            'parameters' => $parameters
        ];

        if ($context) {
            $sentData['context'] = $context;
        }

        $this->sentXml = $sentData;
        $result = [];

        try {
            $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
            $service = ($this->use_without_license || $forceWithoutLicense) ? 'EduLicenseWSConsultaSQL' : 'wsConsultaSQL';
            $this->clientSoap = $services->clientSoap($service);

            $result = $this->executeSql($sentData);
            $result = $this->convertToArray(
                simplexml_load_string(
                    str_replace(
                        ['&#x1F;', '&#x1f;', '&amp;#x1F;', '&amp;#x1f;', '&#xC'],
                        '',
                        $result
                    )
                )
            );
        } catch (\SoapFault $e) {
            $sentData['Function'] = $context ? 'RealizarConsultaSQLContexto' : 'RealizarConsultaSQL';
            $this->errorMsg = $e->getMessage();
            return false;
        }

        return $result;
    }

    public function executeSqlQueryWithoutLicense($codSentenca, $codColigada, $codSistema, $parameters, $context = null)
    {
        return $this->executeSqlQuery($codSentenca, $codColigada, $codSistema, $parameters, $context, true);
    }

    private function executeSql($sentData)
    {
        if (isset($sentData['context'])) {
            return $this->clientSoap->RealizarConsultaSQLContexto($sentData)->RealizarConsultaSQLContextoResult;
        }

        return $this->clientSoap->RealizarConsultaSQL($sentData)->RealizarConsultaSQLResult;
    }

    public function readRecord($dataServerName, $primaryKey, $context, $returnJson = true)
    {
        $resultInArray = [];
        $resultInXml = '';
        $sentData = [
            'DataServerName' => $dataServerName,
            'PrimaryKey' => $primaryKey,
            'Contexto' => $context
        ];

        try {
            $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
            if ($this->use_without_license) {
                $this->clientSoap = $services->clientSoap('EduLicenseWsDataServer');
            } else {
                $this->clientSoap = $services->clientSoap('wsDataServer');
            }
            $result = $this->clientSoap->ReadRecord($sentData);
            $resultInXml = $result->ReadRecordResult;
            $resultInArray = $this->convertToArray(
                simplexml_load_string(
                    str_replace(
                        ['&#x1F;', '&#x1f;', '&amp;#x1F;', '&amp;#x1f;', '&#xC'],
                        '',
                        $result->ReadRecordResult
                    )
                )
            );

            $sentData['Function'] = 'ReadRecord';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'ReadRecord';
            return false;
        }

        if ($returnJson == true) {
            return json_encode($resultInArray);
        } else {
            return $resultInXml;
        }
    }

    public function readView($dataServerName, $filter, $context, $returnJson = true)
    {
        $sentData = [
            'DataServerName' => $dataServerName,
            'Filtro' => $filter,
            'Contexto' => $context
        ];

        $this->sentXml = $sentData;

        try {
            $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
            if ($this->use_without_license) {
                $this->clientSoap = $services->clientSoap('EduLicenseWsDataServer');
            } else {
                $this->clientSoap = $services->clientSoap('wsDataServer');
            }
            $result = $this->clientSoap->ReadView($sentData);
            $result = $this->convertToArray(
                simplexml_load_string(
                    str_replace(
                        ['&#x1F;', '&#x1f;', '&amp;#x1F;', '&amp;#x1f;', '&#xC'],
                        '',
                        $result->ReadViewResult
                    )
                )
            );
        } catch (\SoapFault $e) {
            $this->errorMsg = $e->getMessage();
            return false;
        } finally {
            $sentData['Function'] = 'ReadView';
        }

        if ($returnJson == true) {
            return json_encode($result);
        } else {
            return $result;
        }
    }

    public function getXmlDataServer($dataServerName, $data)
    {
        $xml = new \SimpleXMLElement('<' . $dataServerName . '/>');
        $this->arrayToXml($data, $xml);
        return $xml->asXML();
    }

    public function getSchemaDataServer($dataServerName, $context, $returnJson = false)
    {
        $resultInArray = [];
        $resultInXml = '';
        $sentData = [
            'DataServerName' => $dataServerName,
            'Contexto' => $context
        ];

        $this->sentXml = $sentData;
        try {
            $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
            if ($this->use_without_license) {
                $this->clientSoap = $services->clientSoap('EduLicenseWsDataServer');
            } else {
                $this->clientSoap = $services->clientSoap('wsDataServer');
            }
            $result = $this->clientSoap->GetSchema($sentData);
            $resultInXml = $result->GetSchemaResult;
            $resultInArray = $this->convertSchema(
                simplexml_load_string(
                    str_replace(
                        ['</xs:', '<xs:'],
                        ['</xs', '<xs'],
                        str_replace(
                            ['&#x1F;', '&#x1f;', '&amp;#x1F;', '&amp;#x1f;', '&#xC'],
                            '',
                            $result->GetSchemaResult
                        )
                    )
                )
            );

            $sentData['Function'] = 'GetSchema';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'GetSchema';
            $this->errorMsg = $e->getMessage();
            return false;
        }

        if ($returnJson == true) {
            return json_encode($resultInArray);
        } else {
            return $resultInArray;
        }
    }

    public function saveRecord($dataServerName, $data, $context)
    {
        $structure = $this->getSchemaDataServer($dataServerName, $context);

        if (!$structure) {
            return false;
        }

        $primaryKey = [];
        $primaryKeyComplete = true;

        foreach ($structure['primaryKey'][0]['columns'] as $value) {
            if (isset($data[$structure['primaryKey'][0]['table']][$value])) {
                $primaryKey[] = $data[$structure['primaryKey'][0]['table']][$value];
            } else {
                $primaryKeyComplete = false;
                $data[$structure['primaryKey'][0]['table']][$value] = 0;
            }
        }
        if ($primaryKeyComplete) {
            $primaryKey = implode(';', $primaryKey);
        } else {
            $primaryKey = null;
        }

        $xml = $this->getXmlDataServer($dataServerName, $data);

        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsDataServer');
        } else {
            $this->clientSoap = $services->clientSoap('wsDataServer');
        }

        $sentData = [
            'DataServerName' => $dataServerName,
            'XML' => $xml,
            'Contexto' => $context
        ];
        $this->sentXml = $sentData;
        try {
            $result = $this->clientSoap->SaveRecord($sentData);
            $resultResponse = $result->SaveRecordResult;
            $this->resultResponse = $resultResponse;
            $sentData['Function'] = 'SaveRecord';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'SaveRecord';
            $this->errorMsg = $e->getMessage();

            return false;
        }

        if ((substr_count($resultResponse, ';')  == count($structure['primaryKey'][0]['columns']) - 1) && !strpos($resultResponse, '==========')) {
            $result = explode(';', $resultResponse);
            $primaryKey = [$structure['primaryKey'][0]['table'] => []];
            $primaryKeyComplete = true;
            foreach ($structure['primaryKey'][0]['columns'] as $key => $value) {
                $primaryKey[$structure['primaryKey'][0]['table']][$value] = $result[$key];
            }
            return $primaryKey;
        }

        return false;
    }

    private function replaceVariblesXmlProcess($data, $fileXml)
    {
        $xml = file_get_contents($fileXml);
        foreach ($data as $key => $value) {
            $find[] = '{{' . $key . '}}';
            $repalce[] = $value;
        }
        return str_replace($find, $repalce, $xml);
    }

    /*
        esperar o array com os dados
    */
    public function executeWithXmlParams($processServerName, $data, $fileXml)
    {
        $strXmlParams = $this->replaceVariblesXmlProcess($data, $fileXml);

        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsProcess');
        } else {
            $this->clientSoap = $services->clientSoap('wsProcess');
        }

        $sentData = [
            'ProcessServerName' => $processServerName,
            'strXmlParams' => $strXmlParams
        ];
        $this->sentXml = $sentData;
        try {
            $result = $this->clientSoap->ExecuteWithXmlParams($sentData);
            $resultResponse = $result->ExecuteWithXmlParamsResult;
            $sentData['Function'] = 'ExecuteWithXmlParams';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'ExecuteWithXmlParams';
            $this->errorMsg = $e->getMessage();

            return false;
        }
        return json_encode($resultResponse);
    }

    /*
        esperar o array com os dados
    */
    public function executeProcess($processServerName, $data)
    {
        $resultInXml = Config::getProcessFile($processServerName);
        preg_match_all('|\{\{(.*)\}\}|U', $resultInXml, $field, PREG_PATTERN_ORDER);
        preg_match_all('|\[\[(.*)\]\]|U', $resultInXml, $context, PREG_PATTERN_ORDER);
        $field = $field[1];
        $context = $context[1];

        if (isset($data['times_will_send_substructure'])) {
            $repeatStructure = [];
            $firstHalf = explode('<!-- Repetir estrutura -->', $resultInXml);
            for ($i = 1; $i < count($firstHalf); $i++) {
                $repeatStructure[] = explode('<!-- Fim repetir estrutura -->', $firstHalf[$i])[0];
            }
            $repeatedWithValues = $this->insertMultivaluedFieldsOnRepeatedSubstructure($data['times_will_send_substructure'], $repeatStructure, $field, $data);
            foreach ($repeatStructure as $key => $strucuture) {
                $resultInXml = str_replace($strucuture, $repeatedWithValues[$key], $resultInXml);
            }
            $resultInXml = str_replace(" \r\n ", "", $resultInXml);
        }

        $arrayReplace = ['find' => [], 'replace' => []];
        for ($i = 0; $i < count($context); $i++) {
            $values = explode('|', $context[$i]);
            $arrayReplace['find'][] = '[[' . $context[$i] . ']]';
            $arrayReplace['replace'][] = isset($data['context'][$values[0]]) ? $data['context'][$values[0]] : (isset($values[1]) ? $values[1] : "");
        }

        for ($i = 0; $i < count($field); $i++) {
            $values = explode('|', $field[$i]);
            $arrayReplace['find'][] = '{{' . $field[$i] . '}}';
            $arrayReplace['replace'][] = isset($data['campos'][$values[0]]) ? $data['campos'][$values[0]] : (isset($values[1]) ? $values[1] : "");
        }

        $strXmlParams = str_replace($arrayReplace['find'], $arrayReplace['replace'], $resultInXml);

        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);

        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsProcess');
        } else {
            $this->clientSoap = $services->clientSoap('wsProcess');
        }

        $sentData = [
            'ProcessServerName' => $processServerName,
            'strXmlParams' => $strXmlParams
        ];
        $this->sentXml = $sentData;
        try {
            $type = Config::getTypeProcess($processServerName);
            if ($type == 'Params') {
                $result = $this->clientSoap->ExecuteWithParams($sentData);
                $resultResponse = $result->ExecuteWithParamsResult;
            } else {
                $result = $this->clientSoap->ExecuteWithXmlParams($sentData);
                $resultResponse = $result->ExecuteWithXmlParamsResult;
            }
            $sentData['Function'] = 'ExecuteWithXmlParams';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'ExecuteWithXmlParams';
            $this->errorMsg = $e->getMessage();

            return false;
        }
        return json_encode($resultResponse);
    }

    private function insertMultivaluedFieldsOnRepeatedSubstructure($timesWillRepeat, $repeatingStructures, $fieldToSubstitue, $fieldsWithValue)
    {
        $strXmlParams = [];
        foreach ($repeatingStructures as $key => $structure) {
            for ($j = 0; $j < $timesWillRepeat; $j++) {
                $arrayReplace = ['find' => [], 'replace' => []];
                for ($i = 0; $i < count($fieldToSubstitue); $i++) {
                    $values = explode('|', $fieldToSubstitue[$i]);
                    if (gettype($fieldsWithValue['campos'][$values[0]]) == 'array') {
                        $arrayReplace['find'][] = '{{' . $fieldToSubstitue[$i] . '}}';
                        $arrayReplace['replace'][] = isset($fieldsWithValue['campos'][$values[0]]) ? $fieldsWithValue['campos'][$values[0]][$j] : (isset($values[1]) ? $values[1] : "");
                    }
                }
                $strXmlParams[$key][] = str_replace($arrayReplace['find'], $arrayReplace['replace'], $structure);
            }
        }
        foreach ($strXmlParams as $structureBlock) {
            $imploded[] = implode($structureBlock);
        }
        return $imploded;
    }

    public function executeWithParams($processServerName, $strXmlParams)
    {
        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsProcess');
        } else {
            $this->clientSoap = $services->clientSoap('wsProcess');
        }

        $sentData = [
            'ProcessServerName' => $processServerName,
            'strXmlParams' => $strXmlParams
        ];
        $this->sentXml = $sentData;

        try {
            $result = $this->clientSoap->ExecuteWithParams($sentData);
            $resultResponse = $result->ExecuteWithParamsResult;
            $sentData['Function'] = 'ExecuteWithParams';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'ExecuteWithParams';
            $this->errorMsg = $e->getMessage();

            return false;
        }
        return json_encode($resultResponse);
    }

    public function reportInfo($codColigada, $idReport, $returnJson = false)
    {
        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsReport');
        } else {
            $this->clientSoap = $services->clientSoap('wsReport');
        }

        $sentData = [
            'codColigada' => $codColigada,
            'idReport' => $idReport
        ];

        try {
            $result = $this->clientSoap->GetReportInfo($sentData);
            $resultResponse = $result->GetReportInfoResult;
            $sentData['Function'] = 'GetReportInfo';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'GetReportInfo';

            return false;
        }
        $data = ['filters' => $resultResponse->string[0], 'parameters' => $resultResponse->string[1]];
        if ($returnJson) {
            return json_encode($data);
        }
        return $data;
    }

    public function genereteReport($codColigada, $idReport, $filters, $parameters, $viewType = false, $returnJson = false, string $context = '', bool $forceDowload = false)
    {
        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWsReport');
        } else {
            $this->clientSoap = $services->clientSoap('wsReport');
        }
        $this->sentXml = [
            'codColigada' => $codColigada,
            'id' => $idReport,
            'filters' => $filters,
            'parameters' => $parameters,
            'fileName' => 'relatorio.pdf'
        ];

        if ($context) {
            $this->sentXml['contexto'] = $context;
        }

        try {
            $result = $this->clientSoap->GenerateReport($this->sentXml);
            $sentData['Function'] = 'GetReportInfo';
            $sentData['filters'] = $filters;
            $sentData['parameters'] = $parameters;
            if ($context) {
                $sentData['contexto'] = $context;
            }
            if ($result) {

                $resultSize = $this->clientSoap->GetGeneratedReportSize([
                    'guid' => rtrim($result->GenerateReportResult)
                ]);

                if ($resultSize && ($viewType !== 4 || $forceDowload)) {
                    $resultReport = $this->clientSoap->GetFileChunk([
                        'guid' => rtrim($result->GenerateReportResult),
                        'offset' => 0,
                        'length' => rtrim($resultSize->GetGeneratedReportSizeResult)
                    ]);
                }
            }
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'GetReportInfo';
            $this->errorMsg = $e->getMessage();

            return false;
        }
        $data = [
            'guid' => $result->GenerateReportResult,
            'base64' => $resultReport->GetFileChunkResult ?? null
        ];
        if ($returnJson) {
            return json_encode($data);
        }
        return $data;
    }

    public function getSchemaProcess($processServerName, $context, $returnJson = false)
    {
        $resultInXml = Config::getProcessFile($processServerName);
        preg_match_all('|\{\{(.*)\}\}|U', $resultInXml, $field, PREG_PATTERN_ORDER);
        preg_match_all('|\[\[(.*)\]\]|U', $resultInXml, $context, PREG_PATTERN_ORDER);
        $field = $field[1];
        $context = $context[1];
        $resultInArray = ['campos' => [], 'context' => []];
        for ($i = 0; $i < count($context); $i++) {
            $values = explode('|', $context[$i]);
            $resultInArray['context'][] = [
                'name' => $values[0],
                'default' => isset($values[1]) ? $values[1] : ''
            ];
        }
        for ($i = 0; $i < count($field); $i++) {
            $values = explode('|', $field[$i]);
            $resultInArray['campos'][] = [
                'name' => $values[0],
                'default' => isset($values[1]) ? $values[1] : ''
            ];
        }
        return $resultInArray;
    }

    public function getXmlSchemaProcess($processServerName, $data)
    {
        try {
            $xmlSchemaProcess = Config::getProcessFile($processServerName);
            if (!$xmlSchemaProcess) {
                $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
                if ($this->use_without_license) {
                    $this->clientSoap = $services->clientSoap('EduLicenseWsProcess');
                } else {
                    $this->clientSoap = $services->clientSoap('wsProcess');
                }
                $sentData = [
                    'ProcessServerName' => $processServerName
                ];
                $result = $this->clientSoap->GetSchema($sentData);
                $xmlSchemaProcess = $result->GetSchemaResult;
            }

            $resultInArray = $this->arrayToSchema(
                simplexml_load_string(
                    str_replace(
                        ['</xs:', '<xs:', '<d2p1:', '<a:', '<b:', '</d2p1:', '<d3p1:', '</d3p1:',  '</a:', '</b:', '<diffgr:', '</diffgr:', 'utf-16'],
                        ['</xs', '<xs', '<d2p1', '<a2ponto', '<b2ponto', '</d2p1', '<d3p1', '</d3p1', '</a2ponto', '</b2ponto', '<diffgr2ponto', '</diffgr2ponto', 'utf-8'],
                        str_replace(
                            ['&#x1F;', '&#x1f;', '&amp;#x1F;', '&amp;#x1f;', '&#xC'],
                            '',
                            $xmlSchemaProcess
                        )
                    )
                ),
                $data
            );
            $sentData['Function'] = 'GetSchema';
        } catch (\SoapFault $e) {
            $sentData['Function'] = 'GetSchema';

            return false;
        }
        $removeProperties = Config::getProcessRemoveProperties($processServerName);
        if ($removeProperties) {
            foreach ($removeProperties as $properties) {
                $this->removeProperties($resultInArray, $properties, 0);
            }
        }

        return str_replace(
            ['</xs', '<xs', '<d2p1', '<a2ponto', '<b2ponto', '</d2p1', '<d3p1', '</d3p1', '</a2ponto', '</b2ponto', '<diffgr2ponto',  '</diffgr2ponto', 'utf-8'],
            ['</xs:', '<xs:', '<d2p1:', '<a:', '<b:', '</d2p1:', '<d3p1:', '</d3p1:', '</a:', '</b:', '<diffgr:', '</diffgr:', 'utf-16'],
            $resultInArray->asXML()
        );
    }

    private function removeProperties($result, $properties, $i)
    {
        if (count($properties) > $i + 1) {
            $this->removeProperties($result->{$properties[$i]}, $properties, ++$i);
        } else {
            unset($result->{$properties[$i]});
        }
    }

    public function authenticateAccess()
    {
        $services = new Services($this->tbc, $this->totvs_user, $this->totvs_pass, $this->connection_timeout);
        if ($this->use_without_license) {
            $this->clientSoap = $services->clientSoap('EduLicenseWSConsultaSQL');
        } else {
            $this->clientSoap = $services->clientSoap('wsConsultaSQL');
        }

        try {
            $result = $this->clientSoap->AutenticaAcesso();
            $resultResponse = $result->AutenticaAcessoResult;
        } catch (\SoapFault $e) {

            return false;
        }
        return json_encode($resultResponse);
    }

    private function convertToArray($result)
    {
        if (!empty($result) && !is_null($result)) {
            $resultInArray = $row = array();
            foreach ($result as $rowKey => $rowValue) {
                $row = [];
                foreach ($rowValue as $key => $value) {
                    $row[$key] = trim($value);
                }
                $resultInArray[] = $row;
            }
            return $resultInArray;
        }
        return [];
    }

    private function convertSchema($result)
    {
        $resultStructure = $result->xsschema->xselement->xscomplexType->xschoice->xselement;
        if (!empty($resultStructure) && !is_null($resultStructure)) {
            $resultInArray = ['structure' => [], 'primaryKey' => []];
            $row = array();
            foreach ($resultStructure as $rowKey => $rowValue) {
                $row = [];
                foreach ($rowValue->xscomplexType->xssequence->xselement as $key => $value) {
                    $row[] = trim($value['name']);
                }
                $resultInArray['structure'][] = ['table' => trim($rowValue['name']), 'columns' => $row];
            }
            $resultKeys = $result->xsschema->xselement->xsunique;
            foreach ($resultKeys as $rowKey => $rowValue) {
                $row = [];
                foreach ($rowValue->xsfield as $key => $value) {
                    $row[] = str_replace(['.//', 'mstns:'], '', trim($value['xpath']));
                }
                $resultInArray['primaryKey'][] = ['table' => str_replace(['.//', 'mstns:'], '', trim($rowValue->xsselector['xpath'])), 'columns' => $row];
            }
            return $resultInArray;
        }
        return [];
    }

    private function getFieldProcess($key, $value, &$response)
    {
        if ($value instanceof \SimpleXMLElement && !empty($value) && trim($value) == "") {
            foreach ($value as $k => $v) {
                if (!in_array($k, ['Context', 'PrimaryKeyNames', 'InternalId'])) {
                    $this->getFieldProcess($key . '.' . $k, $v, $response);
                }
            }
        } else {
            $response['campos'][] = ['name' => $key, 'default' => trim($value)];
        }
    }

    private function convertSchemaProcess($result)
    {
        $response = [];
        if (!empty($result) && !is_null($result)) {
            if (isset($result->Context->d2p1_params)) {
                $context = $result->Context->d2p1_params->d3p1KeyValueOfanyTypeanyType;
                foreach ($context as $key => $value) {
                    $response['context'][] = ['name' => trim($value->d3p1Key), 'default' => trim($value->d3p1Value)];
                }
            } elseif (isset($result->Context->a2ponto_params)) {
                $context = $result->Context->a2ponto_params->b2pontoKeyValueOfanyTypeanyType;
                foreach ($context as $key => $value) {
                    $response['context'][] = ['name' => trim($value->b2pontoKey), 'default' => trim($value->b2pontoValue)];
                }
            }
            foreach ($result as $key => $value) {
                if (!in_array($key, ['Context', 'PrimaryKeyNames', 'InternalId'])) {
                    $this->getFieldProcess($key, $value, $response);
                }
            }
        }
        return $response;
    }

    private function setValueFieldProcess($key1, $key, $value, $data, &$xmlSchema)
    {
        if ($value instanceof \SimpleXMLElement && !empty($value) && trim($value) == "") {
            foreach ($value as $k => $v) {
                if (!in_array($k, ['Context', 'PrimaryKeyNames', 'InternalId'])) {
                    $this->setValueFieldProcess($k, $key . '.' . $k, $v, $data, $xmlSchema->{$key1});
                }
            }
        } else {
            if (isset($data[$key])) {
                if (is_array($data[$key])) {
                    foreach ($data[$key] as $k => $v) {
                        $xmlSchema->{$key1}[$k] = $v;
                    }
                } else {
                    $xmlSchema->{$key1} = $data[$key];
                }
            }
        }
    }

    private function arrayToSchema($xmlSchema, $data, $result = null)
    {
        if (!empty($xmlSchema) && !is_null($xmlSchema)) {
            if (isset($result->Context->a2ponto_params)) {
                $context = $xmlSchema->Context->d2p1_params->d3p1KeyValueOfanyTypeanyType;
                foreach ($context as $key => $value) {
                    if (isset($data['contexto'][trim($value->d3p1Key)])) {
                        $xmlSchema->Context->d2p1_params->d3p1KeyValueOfanyTypeanyType->$key->d3p1Value = $data['contexto'][trim($value->d3p1Key)];
                    }
                }
            } elseif (isset($result->Context->a2ponto_params)) {
                $context = $result->Context->a2ponto_params->b2pontoKeyValueOfanyTypeanyType;
                foreach ($context as $key => $value) {
                    $response['context'][] = ['name' => trim($value->b2pontoKey), 'default' => trim($value->b2pontoValue)];
                }
            }
            $xmlSchema2 = clone $xmlSchema;
            foreach ($xmlSchema2 as $key => $value) {
                if (!in_array($key, ['Context', 'PrimaryKeyNames', 'InternalId'])) {
                    $this->setValueFieldProcess($key, $key, $value, $data['campos'], $xmlSchema);
                }
            }
        }
        return $xmlSchema;
    }

    private function arrayToXml($data, &$xmlData)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists(0, $value)) {
                    foreach ($value as $k => $v) {
                        $subnode = $xmlData->addChild($key);
                        $this->arrayToXml($v, $subnode);
                    }
                } else {
                    if (is_numeric($key)) {
                        $key = 'item' . $key;
                    }
                    $subnode = $xmlData->addChild($key);
                    $this->arrayToXml($value, $subnode);
                }
            } else {
                $xmlData->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}
