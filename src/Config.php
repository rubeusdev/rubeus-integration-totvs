<?php

namespace Rubeus\RbIntegrationTotvs\Src;

abstract class Config
{
    private static $processConfig = [
        'EduPSCancelaDistribuicaoCadernoProvaData' => [
            'file' => __DIR__ . '/ProcessFile/EduPSCancelaDistribuicaoCadernoProvaData.xml'
        ],
        'EduPSDistribuicaoCadernoProvaData' => [
            'file' => __DIR__ . '/ProcessFile/EduPSDistribuicaoCadernoProvaData.xml'
        ],
        'FinBoletoInclusaoData' => [
            'file' => __DIR__ . '/ProcessFile/FinBoletoInclusaoData.xml'
        ],
        'EduMatricAprovadosPSData' => [
            'file' => __DIR__ . '/ProcessFile/EduMatricAprovadosPSData.xml'
        ],
        'EduGerarLancFromParcelaData' => [
            'file' => __DIR__ . '/ProcessFile/EduGerarLancFromParcelaData.xml'
        ],
        'EduPSAlteraStatusOpcaoData' => [
            'file' => __DIR__ . '/ProcessFile/EduPSAlteraStatusOpcaoData.xml'
        ],
        'EduPSClassificaCandidatosData' => [
            'file' => __DIR__ . '/ProcessFile/EduPSClassificaCandidatosData.xml'/*,
            'removeProperties' => [
                ['_classificacaoManualData', 'xsschema',  'xselement', 'xscomplexType', 'xschoice']
            ]*/
        ],
        'FinBoletoRegistroOnLineDataProc' =>   [
            'file' => __DIR__ . '/ProcessFile/FinBoletoRegistroOnLineDataProc.xml'
        ],
        'FinLanBaixaTBCData' =>   [
            'file' => __DIR__ . '/ProcessFile/FinLanBaixaTBCParamsProc.xml',
            'type' => 'Params'
        ],
        'GlbWorkflowExecProc' =>   [
            'file' => __DIR__ . '/ProcessFile/GlbWorkflowExecProc.xml'
        ],
        'EduPSChamadaCandidatosData' =>   [
            'file' => __DIR__ . '/ProcessFile/EduPSChamadaCandidatosData.xml'
        ],
        'EduAtualizaRespFinData' =>   [
            'file' => __DIR__ . '/ProcessFile/EduAtualizaRespFinData.xml'
        ],
        'EduPSCancelaInscricaoData' =>   [
            'file' => __DIR__ . '/ProcessFile/EduPSCancelaInscricaoData.xml'
        ],
        'EduPSConfirmaInscricaoData' =>   [
            'file' => __DIR__ . '/ProcessFile/EduPSConfirmaInscricaoData.xml'
        ]
    ];

    public static function setProcessConfig($process, $file)
    {
        self::$processConfig[$process] = $file;
    }

    public static function getProcessFile($process)
    {
        if (isset(self::$processConfig[$process]) && self::$processConfig[$process]) {
            return file_get_contents(self::$processConfig[$process]['file']);
        }
        return false;
    }

    public static function getTypeProcess($process)
    {
        if (isset(self::$processConfig[$process]) && self::$processConfig[$process] && isset(self::$processConfig[$process]['type']) && self::$processConfig[$process]['type']) {
            return self::$processConfig[$process]['type'];
        }
        return false;
    }

    public static function getProcessRemoveProperties($process)
    {
        if (isset(self::$processConfig[$process]) && self::$processConfig[$process]) {
            return self::$processConfig[$process]['removeProperties'];
        }
        return false;
    }
}
