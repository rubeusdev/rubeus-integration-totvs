<?php

namespace Rubeus\RbIntegrationTotvs\Src;


class Services
{
    private $tbc;
    private $totvs_user;
    private $totvs_pass;
    private $connection_timeout;

    public function __construct($tbc, $totvs_user, $totvs_pass, int $connection_timeout = 1000)
    {
        $this->tbc = $tbc;
        $this->totvs_user = $totvs_user;
        $this->totvs_pass = $totvs_pass;
        $this->connection_timeout = $connection_timeout;
    }

    public function clientSoap($service)
    {
        $clientSoap = null;

        try {
            $clientSoap = new \SoapClient(
                $this->tbc . '/' . $service . '/MEX?singleWsdl',
                [
                    'trace' => 1,
                    'exceptions' => true,
                    'stream_context' => stream_context_create(
                        [
                            'ssl' => [
                                'verify_peer'       => false,
                                'verify_peer_name'  => false,
                                'allow_self_signed' => true
                            ]
                        ]
                    ),
                    'cache_wsdl' => WSDL_CACHE_DISK,
                    'login' => $this->totvs_user,
                    'connection_timeout' => $this->connection_timeout,
                    'password' => $this->totvs_pass
                ]
            );
        } catch (\SoapFault $e) {
            throw new \SoapFault('Erro ao conectar ao TBC', $e->getCode());
        }
        return $clientSoap;
    }
}
