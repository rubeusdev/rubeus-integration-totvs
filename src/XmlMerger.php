<?php

namespace Rubeus\RbIntegrationTotvs\Src;

class XmlMerger
{
    public static function mergeXml($xml1, $xml2, $dataServerName)
    {
        $xml1 = self::convertToArrayXml(simplexml_load_string($xml1));
        $xml2 = self::convertToArrayXml(simplexml_load_string($xml2));

        $merged = self::arrayMergeRecursiveDistinct($xml1, $xml2);

        $xmlReturn = new \SimpleXMLElement('<'.$dataServerName.'/>');

        self::arrayToXml($merged, $xmlReturn);

        return $xmlReturn->asXML();
    }

    private static function convertToArrayXml($result)
    {
        if (!empty($result) && !is_null($result)) {
            $resultInArray = $row = array();
            foreach ($result as $rowKey => $rowValue) {
                $row = [];
                foreach ($rowValue as $key => $value) {
                    $row[$key] = trim($value);
                }
                $resultInArray[$rowKey] = $row;
            }
            return $resultInArray;
        }
        return [];
    }

    private static function arrayMergeRecursiveDistinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = self::arrayMergeRecursiveDistinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }
        $merged = self::replaceDotForComma($merged);
        return $merged;
    }

    private static function arrayToXml($array, &$xmlReturn)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric(($key))) {
                    $subnode = $xmlReturn->addChild($key);
                    self::arrayToXml($value, $subnode);
                } else {
                    self::arrayToXml($value, $subnode);
                }
            } else {
                $xmlReturn->addChild($key, $value);
            }
        }
    }

    public static function replaceDotForComma(&$input)
    {
        if (is_array($input)) {
            foreach ($input as &$value) {
                self::replaceDotForComma($value);
            }
        } else {
            if (is_numeric($input)) {
                $input = str_replace(',', '', $input);
                $input = str_replace('.', ',', $input);
            }
        }
        return $input;
    }
}
